# Project Title

Veratrak technical test REST API

## Description

* For the implementation of this task Express.js framework running on the Node.js was used.
* View engine for Express.js was defined for express handlebars.
* In order to simplify the process of creating of the project build-in express-generator plug-in was used.
* API consists of two end points: to insert data, and to retrieve all data from the database collection.
* Data can be added through UI form.

## Getting Started

### Dependencies

* NodeJS
* Mongoose
* ExpressJS
* Express Handlebars

### Instructions for running the application

1. Open terminal window and navigate to the folder containing project files.
2. Run the MongoDB server.
    
	2.1. Navigate to the mongoDB server folder, e.g. 'C:\Program Files\MongoDB\Server\4.0\bin\'
    
	2.2. Run following command: 'mongod.exe'
	
3. Run the following commands:
    
	3.1. 'npm install' - in order to download all npm dependencies
    
	3.2. 'node bin/www'
	
4. Server will start it's work.
5. Open browser window and navigate to the page 'localhost:8000'.

## Author
Joanna Kiedrowska
email: joanna.kiedrowska95@gmail.com