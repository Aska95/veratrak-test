var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    console.log("Connection Successful!");

    var Schema = mongoose.Schema;

    var medicinesSchema = new Schema({
        name: String,
        manufacturer: String,
        amount_in_stock: Number
    }, {collection: 'medicines'});

    var MedicinesData = mongoose.model('MedicinesData', medicinesSchema);

    /* GET home page. */
    router.get('/', function (req, res, next) {
        res.render('index');
    });

    router.get('/get-data', function (req, res, next) {
        MedicinesData.find().lean().exec(function (err, medicines) {
            return res.end(JSON.stringify(medicines));
        });
    });

    router.post('/insert', function (req, res, next) {
        var item = {
            name: req.body.name,
            manufacturer: req.body.manufacturer,
            amount_in_stock: req.body.amount
        };

        var data = new MedicinesData(item);
        data.save(function (err, medicine) {
            if (err) return console.error(err);
            console.log(medicine.name + " saved to medicines collection.");
        });
        res.redirect('/');
    });

});

module.exports = router;